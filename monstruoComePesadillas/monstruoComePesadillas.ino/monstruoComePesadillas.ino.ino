const int relayPin = 2;
float startTime;
bool timerEnd;
float endTime;

void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);
  pinMode(relayPin, OUTPUT);
  endTime = 12000;
  timerEnd = false;
  startTime = millis();
}

void loop() {
  int data = Serial.read();

  if ( data == 0) {
    timerEnd = false;
    startTime = millis();
  }

  float timer = millis() - startTime;

  if (timer >= endTime) {
    timerEnd = true;
  }
  if (timer <= 12000) {
    digitalWrite(relayPin, HIGH);
    //Serial.println(timer);
  } else {
    digitalWrite(relayPin, LOW);
  }

  Serial.println(data);

}
